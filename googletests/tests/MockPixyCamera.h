/*
 * MockPixyCamera.h
 *
 *  Created on: Feb 9, 2017
 *      Author: robotics
 */

#ifndef TESTS_MOCKPIXYCAMERA_H_
#define TESTS_MOCKPIXYCAMERA_H_

#include "gmock/gmock.h"
#include "PixyCamera/PixyCamera.h"

class MockPixyCamera : public PixyCamera
{
public:
	MOCK_METHOD0(GetBlocks, std::vector<Block>());
};



#endif /* TESTS_MOCKPIXYCAMERA_H_ */
