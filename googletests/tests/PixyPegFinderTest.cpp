/*
 * PixyPegFinder.cpp
 *
 *  Created on: Feb 9, 2017
 *      Author: robotics
 */

#include "gtest/gtest.h"
#include "MockPixyCamera.h"
#include "PegFinder/PixyPegFinder.h"
#include <stdint.h>
#include <memory>

using namespace testing;

static const uint16_t kPegBlockSignature(3);

TEST(PixyPegFinder, Construct)
{
	std::shared_ptr<MockPixyCamera> mockCam = std::make_shared<MockPixyCamera>();
	PixyPegFinder pegFinder(mockCam, kPegBlockSignature);
}

class PixyPegFinder_fixture : public Test
{
public:
	PixyPegFinder_fixture() :
		mockCam (std::make_shared<MockPixyCamera>()),
		pegFinder (mockCam, kPegBlockSignature),
		testBlocks ()
	{}

	void SetUp() override
	{
	}

	void StageBlocksAndVerifyBlockVisible()
	{
		EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
		EXPECT_TRUE(pegFinder.isBlockVisible());
	}

	void StageBlocksAndVerifyBlockNotVisible()
	{
		EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
		EXPECT_FALSE(pegFinder.isBlockVisible());
	}

	void StageBlocksAndVerifySideStripVisible()
	{
		EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
		EXPECT_TRUE(pegFinder.isSideStripVisible());
	}

	void StageBlocksAndVerifySideStripNotVisible()
	{
		EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
		EXPECT_FALSE(pegFinder.isSideStripVisible());
	}

	void addCenteredBlock(uint16_t signature)
	{
		testBlocks.push_back(PixyCamera::Block());
		testBlocks.back().signature = signature;
		testBlocks.back().y = 120;
		testBlocks.back().x = 160;
	}

	struct Meters {
		explicit Meters(double _meters) : meters(_meters) {}
		double get() const { return meters; }
		const double meters;
	};

	void addBlockAtDistance(const Meters meters)
	{
		// TODO: Tie this to the values in PixyCamera.cpp.
		const double height1m = 29.0, width1m = 11.0;
		addCenteredBlock(kPegBlockSignature);
		testBlocks.back().height = height1m / meters.get() + 0.5; // Add 0.5 to round to nearest
		testBlocks.back().width = width1m / meters.get() + 0.5; // Add 0.5 to round to nearest
	}

protected:
	std::shared_ptr<MockPixyCamera> mockCam;
	PixyPegFinder pegFinder;
	std::vector<PixyCamera::Block> testBlocks;
};


TEST_F(PixyPegFinder_fixture, isBlockVisible_0Blocks)
{
	StageBlocksAndVerifyBlockNotVisible();
}

TEST_F(PixyPegFinder_fixture, isBlockVisible_1MatchingBlock)
{
	addCenteredBlock(kPegBlockSignature);
	StageBlocksAndVerifyBlockVisible();
}

TEST_F(PixyPegFinder_fixture, isBlockVisible_1MatchingBlockBeforeMismatchingBlock)
{
	addCenteredBlock(kPegBlockSignature);
	addCenteredBlock(kPegBlockSignature + 1);
	StageBlocksAndVerifyBlockVisible();
}

TEST_F(PixyPegFinder_fixture, isBlockVisible_1MatchingBlockAfterMismatchingBlock)
{
	addCenteredBlock(kPegBlockSignature + 1);
	addCenteredBlock(kPegBlockSignature);
	StageBlocksAndVerifyBlockVisible();
}

TEST_F(PixyPegFinder_fixture, isBlockVisible_2MatchingBlocks)
{
	addCenteredBlock(kPegBlockSignature);
	addCenteredBlock(kPegBlockSignature);
	StageBlocksAndVerifyBlockVisible();
}

TEST_F(PixyPegFinder_fixture, isBlockVisible_1MismatchingBlock)
{
	addCenteredBlock(kPegBlockSignature + 1);
	StageBlocksAndVerifyBlockNotVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_0Blocks)
{
	StageBlocksAndVerifySideStripNotVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_1Blocks_zeroSize)
{
	addCenteredBlock(kPegBlockSignature);
	StageBlocksAndVerifySideStripNotVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_1SideStrip_1m)
{
	addBlockAtDistance(Meters(1.0));
	StageBlocksAndVerifySideStripVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_1SideStrip_2m)
{
	addBlockAtDistance(Meters(2.0));
	StageBlocksAndVerifySideStripVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_1SideStrip_3m)
{
	addBlockAtDistance(Meters(3.0));
	StageBlocksAndVerifySideStripVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_1SideStrip_half_meter)
{
	addBlockAtDistance(Meters(0.5));
	StageBlocksAndVerifySideStripVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_1SideStrip_weird_meter)
{
	addBlockAtDistance(Meters(0.6242079));
	StageBlocksAndVerifySideStripVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_1SideStrip_1m_slot1)
{
	addCenteredBlock(kPegBlockSignature);
	addBlockAtDistance(Meters(1.0));
	StageBlocksAndVerifySideStripVisible();
}

TEST_F(PixyPegFinder_fixture, isSideStripVisible_horizontalStrip)
{
	// Mimic a boiler strip, which would be short and wide.
	addCenteredBlock(kPegBlockSignature);
	testBlocks.back().width = 30;
	testBlocks.back().height = 5;
	StageBlocksAndVerifySideStripNotVisible();
}

TEST_F(PixyPegFinder_fixture, getDistance_1m)
{
	addBlockAtDistance(Meters(1.0));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(1.0, pegFinder.getPegDistanceInMeters());
}
TEST_F(PixyPegFinder_fixture, getDistance_2m)
{
	addBlockAtDistance(Meters(2.0));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	double distance = pegFinder.getPegDistanceInMeters();
	EXPECT_LE(1.9, distance);
	EXPECT_GE(2.1, distance);
}

TEST_F(PixyPegFinder_fixture, getDistance_halfMeter)
{
	addBlockAtDistance(Meters(0.5));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	double distance = pegFinder.getPegDistanceInMeters();
	EXPECT_LE(0.45, distance);
	EXPECT_GE(0.55, distance);
}

TEST_F(PixyPegFinder_fixture, getDistance_WEIRDmeter)
{
	addBlockAtDistance(Meters(0.6242079));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	double distance = pegFinder.getPegDistanceInMeters();
	EXPECT_LE(.6, distance);
	EXPECT_GE(0.65, distance);
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_0Blocks)
{
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_TRUE(pegFinder.getValidPegStrips().empty());
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_1Block)
{
	addBlockAtDistance(Meters(1.0));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(1u, pegFinder.getValidPegStrips().size());
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_1BlockBadYHigh)
{
	addBlockAtDistance(Meters(1.0));
	testBlocks.back().y = 141;
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(0u, pegFinder.getValidPegStrips().size());
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_1BlockBadYLow)
{
	addBlockAtDistance(Meters(1.0));
	testBlocks.back().y = 99;
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(0u, pegFinder.getValidPegStrips().size());
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_2Blocks)
{
	addBlockAtDistance(Meters(1.0));
	addBlockAtDistance(Meters(1.0));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(2u, pegFinder.getValidPegStrips().size());
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_3Blocks)
{
	addBlockAtDistance(Meters(1.0));
	addBlockAtDistance(Meters(1.0));
	addBlockAtDistance(Meters(1.0));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(3u, pegFinder.getValidPegStrips().size());
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_2BlocksAnd1BadSig)
{
	addCenteredBlock(kPegBlockSignature + 1);
	addBlockAtDistance(Meters(1.0));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(1u, pegFinder.getValidPegStrips().size());
}

TEST_F(PixyPegFinder_fixture, getValidPegStrips_3BlocksAnd1BadSig)
{
	addBlockAtDistance(Meters(1.0));
	addCenteredBlock(kPegBlockSignature + 1);
	addBlockAtDistance(Meters(1.0));
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(2u, pegFinder.getValidPegStrips().size());
}

TEST_F(PixyPegFinder_fixture, findPegStripPair_0Pegs)
{
	EXPECT_CALL(*mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_TRUE(pegFinder.getValidPegStrips().empty());
}

TEST_F(PixyPegFinder_fixture, isYInRange_notInRange)
{
	EXPECT_FALSE(pegFinder.isYInRange(0));
	EXPECT_FALSE(pegFinder.isYInRange(99));
	EXPECT_TRUE(pegFinder.isYInRange(100));
	EXPECT_TRUE(pegFinder.isYInRange(120));
	EXPECT_TRUE(pegFinder.isYInRange(140));
	EXPECT_FALSE(pegFinder.isYInRange(141));
	EXPECT_FALSE(pegFinder.isYInRange(UINT16_MAX));
}

TEST_F(PixyPegFinder_fixture, isAPair_EqualBlocks)
{
	addBlockAtDistance(Meters(1.0));
	PixyCamera::Block block2 = testBlocks.back();

	EXPECT_FALSE(pegFinder.isAPair(testBlocks.back(), block2));
}

TEST_F(PixyPegFinder_fixture, isAPair_DifferentWidths)
{
	addBlockAtDistance(Meters(1.0));
	PixyCamera::Block block2 = testBlocks.back();

	// Shift left the expected distance to make a matching pair.
	block2.x -= double(4.125 * block2.width);

	EXPECT_TRUE(pegFinder.isAPair(testBlocks.back(), block2));
}

