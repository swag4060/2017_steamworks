#include "gtest/gtest.h"
#include "MockPixyCamera.h"
#include <stdint.h>

using namespace testing;

TEST(SanityTest, HelloWorld)
{
	int a = 5;
	EXPECT_EQ(5, a);
}

TEST(MockPixyCamera, Construct)
{
	MockPixyCamera mockCam;
	std::vector<PixyCamera::Block> testBlocks(2);
	EXPECT_CALL(mockCam, GetBlocks()).WillOnce(Return(testBlocks));
	EXPECT_EQ(2u, mockCam.GetBlocks().size());
}
