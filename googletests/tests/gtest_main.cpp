// gtest_main.cpp
#include <stdio.h>
#include "gmock/gmock.h"
#include "gtest/gtest.h"

GTEST_API_ int main(int argc, char **argv) {
  printf("Running main() from gtest_main.cc\n");
  testing::InitGoogleMock(&argc, argv);
  return RUN_ALL_TESTS();
}
