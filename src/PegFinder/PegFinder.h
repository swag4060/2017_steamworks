/*
 * isPegThere.h
 *
 *  Created on: Feb 7, 2017
 *      Author: robotics
 */

#ifndef SRC_PEGFINDER_PEGFINDER_H_
#define SRC_PEGFINDER_PEGFINDER_H_

class PegFinder
{
public:
	virtual ~PegFinder ()
	{}
	virtual bool isPegVisible() = 0;
	virtual double getPegLateralOffsetInMeters() = 0;
	virtual double getPegAngle() = 0;
	virtual double getPegDistanceInMeters() = 0;

private:

};

#endif /* SRC_PEGFINDER_PEGFINDER_H_ */
