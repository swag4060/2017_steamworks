/*
 * PixyPegFinder.cpp
 *
 *  Created on: Feb 9, 2017
 *      Author: robotics
 */

#include "PixyPegFinder.h"
#include <cmath>
#include <exception>

static double s_getBlockHeightWidthRatio(const PixyCamera::Block& block)
{
	return double(block.height) / double(block.width);
}

// Ideal 2"x5" vertical strip at one meter
static PixyCamera::Block s_oneMeterReferenceBlock()
{
	// TODO: This might not be right. We should just set up a test and see what it really looks like.
	// A picture is worth a thousand words!
	PixyCamera::Block reference;
	reference.height = 29.0;
	reference.width = 11;
	return reference;
};
static const PixyCamera::Block kOneMeterReferenceBlock(s_oneMeterReferenceBlock());
static const double kPegStripHeightWidthRatio = s_getBlockHeightWidthRatio(kOneMeterReferenceBlock);

PixyPegFinder::PixyPegFinder(std::shared_ptr<PixyCamera> pixyCam, uint16_t pegSignature)
: m_pixyCam(pixyCam),
  m_pegSignature(pegSignature)
{
}

PixyPegFinder::~PixyPegFinder ()
{
}

bool PixyPegFinder::isBlockVisible()
{
	for (const auto& block : m_pixyCam->GetBlocks())
	{
		if (block.signature == m_pegSignature) return true;
	}
	return false;
}

bool PixyPegFinder::isBlockAPegStrip(const PixyCamera::Block& block)
{
	if (block.signature != m_pegSignature) return false;

	const double blockRatio = s_getBlockHeightWidthRatio(block);

	double matchError = blockRatio / kPegStripHeightWidthRatio - 1.0;

	const double kMatchErrorThreshold = 0.2;
	return std::fabs(matchError) <= kMatchErrorThreshold;
}

bool PixyPegFinder::isSideStripVisible()
{
	for (const auto& block : m_pixyCam->GetBlocks())
	{
		if (isBlockAPegStrip(block))
		{
			return true;
		}
	}
	return false;
}

bool PixyPegFinder::isPegVisible()
{
	return false;
}

double PixyPegFinder::getPegLateralOffsetInMeters()
{
	return 0.0;
}

double PixyPegFinder::getPegAngle()
{
	return 0.0;
}

double PixyPegFinder::getPegDistanceInMeters()
{
	try
	{
		const auto block = getBestBlock();
		double meters = double (kOneMeterReferenceBlock.height) / (block.height);
		return meters;

	}
	catch (const std::exception&)
	{
		return -1.0;
	}
}
PixyCamera::Block PixyPegFinder::getBestBlock()
{
	for (const auto& block : m_pixyCam->GetBlocks())
	{
		if (isBlockAPegStrip(block))
		{
			return block;
		}
	}
	throw std::exception();
}

bool PixyPegFinder::isYInRange(uint16_t y)
{
	uint16_t yMin = 100, yMax = 140;

	return y <= yMax && y >= yMin;
}

std::vector<PixyCamera::Block> PixyPegFinder::getValidPegStrips()
{
	std::vector<PixyCamera::Block> filteredBlocks;

	for (const auto& block : m_pixyCam->GetBlocks())
	{
		if (isBlockAPegStrip(block) && isYInRange(block.y))
		{
			filteredBlocks.push_back(block);
		}
	}
	return filteredBlocks;
}

double PixyPegFinder::findPegStripPair()
{
	std::vector<PixyCamera::Block> pairedBlocks;

	for (const auto& block : m_pixyCam->GetBlocks())
	{
		std::pair<PixyCamera::Block, PixyCamera::Block> blockPair(block, block);
	}
	return 0.0;
}

bool PixyPegFinder::isAPair(const PixyCamera::Block& block1, const PixyCamera::Block& block2)
{
	return block1 != block2;
}
