/*
 * PixyPegFinder.h
 *
 *  Created on: Feb 9, 2017
 *      Author: robotics
 */

#ifndef SRC_PEGFINDER_PIXYPEGFINDER_H_
#define SRC_PEGFINDER_PIXYPEGFINDER_H_

#include "PegFinder.h"
#include "PixyCamera/PixyCamera.h"
#include <memory>
#include <vector>

class PixyPegFinder : public PegFinder
{
public:
	PixyPegFinder(std::shared_ptr<PixyCamera> pixyCam, uint16_t signatureNumber);
	virtual ~PixyPegFinder ();

	virtual bool isBlockVisible(); // returns true if the signature is seen at all.
	virtual bool isSideStripVisible(); // returns true if a block looks like one of the reflective strips
	virtual bool isPegVisible();
	virtual double getPegLateralOffsetInMeters();
	virtual double getPegAngle();
	virtual double getPegDistanceInMeters();
	double findPegStripPair();
	std::vector<PixyCamera::Block> getValidPegStrips();

	static bool isYInRange(uint16_t y);
	static bool isAPair(const PixyCamera::Block& block1, const PixyCamera::Block& block2);

private:
	PixyCamera::Block getBestBlock();
	bool isBlockAPegStrip(const PixyCamera::Block& block);

	std::shared_ptr<PixyCamera> m_pixyCam;
	uint16_t m_pegSignature;
	double m_blockCount();

};




#endif /* SRC_PEGFINDER_PIXYPEGFINDER_H_ */
