/*
 * SpiPixyCamera.cpp
 *
 *  Created on: Jan 31, 2017
 *      Author: robotics
 */

#include <PixyCamera/I2CPixyCamera.h>
#include "Pixy_Lib/Pixy.h"

I2CPixyCamera::I2CPixyCamera()
: m_pixy (std::make_shared<Pixy>())
{
}

I2CPixyCamera::~I2CPixyCamera()
{
}

std::vector<PixyCamera::Block> I2CPixyCamera::GetBlocks()
{
	const uint16_t kMaxBlocks = 100;
	uint16_t validBlockCount = m_pixy->getBlocks(kMaxBlocks);

	std::vector<PixyCamera::Block> validBlocks(validBlockCount);

	const Pixy::Block * pixyBlocks = m_pixy->getBlockArrayPointer();

	for (uint16_t i = 0; i < validBlockCount; ++i)
	{
		validBlocks[i].signature = pixyBlocks[i].signature;
		validBlocks[i].x = pixyBlocks[i].x;
		validBlocks[i].y = pixyBlocks[i].y;
		validBlocks[i].width = pixyBlocks[i].width;
		validBlocks[i].height = pixyBlocks[i].height;
		validBlocks[i].angle = pixyBlocks[i].angle;
	}
	return validBlocks;
}


