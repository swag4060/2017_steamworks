/*
 * SpiPixyCamera.h
 *
 *  Created on: Jan 31, 2017
 *      Author: robotics
 */

#ifndef SRC_PIXYCAMERA_I2CPIXYCAMERA_H_
#define SRC_PIXYCAMERA_I2CPIXYCAMERA_H_

#include "PixyCamera.h"
#include <memory>

class Pixy;

class I2CPixyCamera : public PixyCamera
{
public:
	class Block
	{
	};

	I2CPixyCamera();

	virtual ~I2CPixyCamera() override;

	virtual std::vector<PixyCamera::Block> GetBlocks() override;

private:
	std::shared_ptr<Pixy> m_pixy;

};

#endif /* SRC_PIXYCAMERA_I2CPIXYCAMERA_H_ */
