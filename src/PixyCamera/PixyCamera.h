/*
 * PixyCamera.h
 *
 *  Created on: Jan 31, 2017
 *      Author: robotics
 */

#ifndef SRC_PIXYCAMERA_PIXYCAMERA_H_
#define SRC_PIXYCAMERA_PIXYCAMERA_H_

#include <cstdint>
#include <vector>

class PixyCamera
{
public:

	struct Block
	{
		Block()
		: signature (0),
		  x (0),
		  y (0),
		  width (0),
		  height (0),
		  angle (0)
		{}

		uint16_t signature; //Identification number for your object - you could set it in the pixymon
		uint16_t x; //0 pixel - 320 pixel
		uint16_t y; //0 pixel - 200 pixel
		uint16_t width;
		uint16_t height;
		uint16_t angle; //Only appears when using Color Code

		bool operator==(const Block& rhs) const
		{
			return signature == rhs.signature &&
				   x == rhs.x &&
				   y == rhs.y &&
				   width == rhs.width &&
				   height == rhs.height &&
				   angle == rhs.angle;
		}
		bool operator!=(const Block& rhs) const
		{
			return !(*this == rhs);
		}
	};


	virtual ~PixyCamera()
	{}

	virtual std::vector<Block> GetBlocks() = 0;

};




#endif /* SRC_PIXYCAMERA_PIXYCAMERA_H_ */
